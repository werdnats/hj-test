// Piemenu needs refactoring

var piemenu = new wheelnav('piemenu');
piemenu.clockwise = false;
piemenu.wheelRadius = piemenu.wheelRadius * 0.83;
piemenu.createWheel();
piemenu.setTooltips(['check','image','checkbox','checked','star','icons','download']);

function wheelNav1() {
	jQuery('.testing__title').text('APPLICATION TESTING');
	jQuery('.testing__description').text('Application Security Assessment Services are individually tailored to the customer. The scope of the testing can range from a three day remote web application test to many weeks of on-site, detailed investigation into one aspect of an application by a team of three or four consultants.');
}
function wheelNav2() {
	jQuery('.testing__title').text('TITLE 2');
	jQuery('.testing__description').text('But man is not made for defeat. A man can be destroyed but not defeated.');
}
function wheelNav3() {
	jQuery('.testing__title').text('TITLE 3');
	jQuery('.testing__description').text('Think in the morning. Act in the noon. Eat in the evening. Sleep in the night.');
}
function wheelNav4() {
	jQuery('.testing__title').text('TITLE 4');
	jQuery('.testing__description').text('Permanence, perseverance and persistence in spite of all obstacles, discouragements, and impossibilities: It is this, that in all things distinguishes the strong soul from the weak.');
}
function wheelNav5() {
	jQuery('.testing__title').text('TITLE 5');
	jQuery('.testing__description').text('Keep your face always toward the sunshine - and shadows will fall behind you.');
}
function wheelNav6() {
	jQuery('.testing__title').text('TITLE 6');
	jQuery('.testing__description').text('There is nothing on this earth more to be prized than true friendship.');
}
function wheelNav7() {
	jQuery('.testing__title').text('TITLE 7');
	jQuery('.testing__description').text('Today you are you! That is truer than true! There is no one alive who is you-er than you!');
}

jQuery(document).ready(function($) {
	$('#triggerSearch').click(function(){
		alert("TODO: Write search functionality");
	});

	// Carousels
	$('.carousel--twitter').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
		dots: true,
		adaptiveHeight: false
	});
	$('.accreditations .row').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
		dots: false,
		adaptiveHeight: false,
		arrows: false,
		infinite: true,
		responsive: [
			{
				breakpoint: 1369,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
				}
			},
			{
				breakpoint: 960,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 720,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 540,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
});
