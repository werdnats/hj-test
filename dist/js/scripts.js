// Piemenu needs refactoring

var piemenu = new wheelnav('piemenu');
piemenu.clockwise = false;
piemenu.wheelRadius = piemenu.wheelRadius * 0.83;
piemenu.createWheel();
piemenu.setTooltips(['check','image','checkbox','checked','star','icons','download']);

function wheelNav1() {
	jQuery('.testing__title').text('APPLICATION TESTING');
	jQuery('.testing__description').text('Application Security Assessment Services are individually tailored to the customer. The scope of the testing can range from a three day remote web application test to many weeks of on-site, detailed investigation into one aspect of an application by a team of three or four consultants.');
}
function wheelNav2() {
	jQuery('.testing__title').text('TITLE 2');
	jQuery('.testing__description').text('But man is not made for defeat. A man can be destroyed but not defeated.');
}
function wheelNav3() {
	jQuery('.testing__title').text('TITLE 3');
	jQuery('.testing__description').text('Think in the morning. Act in the noon. Eat in the evening. Sleep in the night.');
}
function wheelNav4() {
	jQuery('.testing__title').text('TITLE 4');
	jQuery('.testing__description').text('Permanence, perseverance and persistence in spite of all obstacles, discouragements, and impossibilities: It is this, that in all things distinguishes the strong soul from the weak.');
}
function wheelNav5() {
	jQuery('.testing__title').text('TITLE 5');
	jQuery('.testing__description').text('Keep your face always toward the sunshine - and shadows will fall behind you.');
}
function wheelNav6() {
	jQuery('.testing__title').text('TITLE 6');
	jQuery('.testing__description').text('There is nothing on this earth more to be prized than true friendship.');
}
function wheelNav7() {
	jQuery('.testing__title').text('TITLE 7');
	jQuery('.testing__description').text('Today you are you! That is truer than true! There is no one alive who is you-er than you!');
}

jQuery(document).ready(function($) {
	$('#triggerSearch').click(function(){
		alert("TODO: Write search functionality");
	});

	// Carousels
	$('.carousel--twitter').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
		dots: true,
		adaptiveHeight: false
	});
	$('.accreditations .row').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
		dots: false,
		adaptiveHeight: false,
		arrows: false,
		infinite: true,
		responsive: [
			{
				breakpoint: 1369,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
				}
			},
			{
				breakpoint: 960,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 720,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 540,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNjcmlwdC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoic2NyaXB0cy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIFBpZW1lbnUgbmVlZHMgcmVmYWN0b3JpbmdcblxudmFyIHBpZW1lbnUgPSBuZXcgd2hlZWxuYXYoJ3BpZW1lbnUnKTtcbnBpZW1lbnUuY2xvY2t3aXNlID0gZmFsc2U7XG5waWVtZW51LndoZWVsUmFkaXVzID0gcGllbWVudS53aGVlbFJhZGl1cyAqIDAuODM7XG5waWVtZW51LmNyZWF0ZVdoZWVsKCk7XG5waWVtZW51LnNldFRvb2x0aXBzKFsnY2hlY2snLCdpbWFnZScsJ2NoZWNrYm94JywnY2hlY2tlZCcsJ3N0YXInLCdpY29ucycsJ2Rvd25sb2FkJ10pO1xuXG5mdW5jdGlvbiB3aGVlbE5hdjEoKSB7XG5cdGpRdWVyeSgnLnRlc3RpbmdfX3RpdGxlJykudGV4dCgnQVBQTElDQVRJT04gVEVTVElORycpO1xuXHRqUXVlcnkoJy50ZXN0aW5nX19kZXNjcmlwdGlvbicpLnRleHQoJ0FwcGxpY2F0aW9uIFNlY3VyaXR5IEFzc2Vzc21lbnQgU2VydmljZXMgYXJlIGluZGl2aWR1YWxseSB0YWlsb3JlZCB0byB0aGUgY3VzdG9tZXIuIFRoZSBzY29wZSBvZiB0aGUgdGVzdGluZyBjYW4gcmFuZ2UgZnJvbSBhIHRocmVlIGRheSByZW1vdGUgd2ViIGFwcGxpY2F0aW9uIHRlc3QgdG8gbWFueSB3ZWVrcyBvZiBvbi1zaXRlLCBkZXRhaWxlZCBpbnZlc3RpZ2F0aW9uIGludG8gb25lIGFzcGVjdCBvZiBhbiBhcHBsaWNhdGlvbiBieSBhIHRlYW0gb2YgdGhyZWUgb3IgZm91ciBjb25zdWx0YW50cy4nKTtcbn1cbmZ1bmN0aW9uIHdoZWVsTmF2MigpIHtcblx0alF1ZXJ5KCcudGVzdGluZ19fdGl0bGUnKS50ZXh0KCdUSVRMRSAyJyk7XG5cdGpRdWVyeSgnLnRlc3RpbmdfX2Rlc2NyaXB0aW9uJykudGV4dCgnQnV0IG1hbiBpcyBub3QgbWFkZSBmb3IgZGVmZWF0LiBBIG1hbiBjYW4gYmUgZGVzdHJveWVkIGJ1dCBub3QgZGVmZWF0ZWQuJyk7XG59XG5mdW5jdGlvbiB3aGVlbE5hdjMoKSB7XG5cdGpRdWVyeSgnLnRlc3RpbmdfX3RpdGxlJykudGV4dCgnVElUTEUgMycpO1xuXHRqUXVlcnkoJy50ZXN0aW5nX19kZXNjcmlwdGlvbicpLnRleHQoJ1RoaW5rIGluIHRoZSBtb3JuaW5nLiBBY3QgaW4gdGhlIG5vb24uIEVhdCBpbiB0aGUgZXZlbmluZy4gU2xlZXAgaW4gdGhlIG5pZ2h0LicpO1xufVxuZnVuY3Rpb24gd2hlZWxOYXY0KCkge1xuXHRqUXVlcnkoJy50ZXN0aW5nX190aXRsZScpLnRleHQoJ1RJVExFIDQnKTtcblx0alF1ZXJ5KCcudGVzdGluZ19fZGVzY3JpcHRpb24nKS50ZXh0KCdQZXJtYW5lbmNlLCBwZXJzZXZlcmFuY2UgYW5kIHBlcnNpc3RlbmNlIGluIHNwaXRlIG9mIGFsbCBvYnN0YWNsZXMsIGRpc2NvdXJhZ2VtZW50cywgYW5kIGltcG9zc2liaWxpdGllczogSXQgaXMgdGhpcywgdGhhdCBpbiBhbGwgdGhpbmdzIGRpc3Rpbmd1aXNoZXMgdGhlIHN0cm9uZyBzb3VsIGZyb20gdGhlIHdlYWsuJyk7XG59XG5mdW5jdGlvbiB3aGVlbE5hdjUoKSB7XG5cdGpRdWVyeSgnLnRlc3RpbmdfX3RpdGxlJykudGV4dCgnVElUTEUgNScpO1xuXHRqUXVlcnkoJy50ZXN0aW5nX19kZXNjcmlwdGlvbicpLnRleHQoJ0tlZXAgeW91ciBmYWNlIGFsd2F5cyB0b3dhcmQgdGhlIHN1bnNoaW5lIC0gYW5kIHNoYWRvd3Mgd2lsbCBmYWxsIGJlaGluZCB5b3UuJyk7XG59XG5mdW5jdGlvbiB3aGVlbE5hdjYoKSB7XG5cdGpRdWVyeSgnLnRlc3RpbmdfX3RpdGxlJykudGV4dCgnVElUTEUgNicpO1xuXHRqUXVlcnkoJy50ZXN0aW5nX19kZXNjcmlwdGlvbicpLnRleHQoJ1RoZXJlIGlzIG5vdGhpbmcgb24gdGhpcyBlYXJ0aCBtb3JlIHRvIGJlIHByaXplZCB0aGFuIHRydWUgZnJpZW5kc2hpcC4nKTtcbn1cbmZ1bmN0aW9uIHdoZWVsTmF2NygpIHtcblx0alF1ZXJ5KCcudGVzdGluZ19fdGl0bGUnKS50ZXh0KCdUSVRMRSA3Jyk7XG5cdGpRdWVyeSgnLnRlc3RpbmdfX2Rlc2NyaXB0aW9uJykudGV4dCgnVG9kYXkgeW91IGFyZSB5b3UhIFRoYXQgaXMgdHJ1ZXIgdGhhbiB0cnVlISBUaGVyZSBpcyBubyBvbmUgYWxpdmUgd2hvIGlzIHlvdS1lciB0aGFuIHlvdSEnKTtcbn1cblxualF1ZXJ5KGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigkKSB7XG5cdCQoJyN0cmlnZ2VyU2VhcmNoJykuY2xpY2soZnVuY3Rpb24oKXtcblx0XHRhbGVydChcIlRPRE86IFdyaXRlIHNlYXJjaCBmdW5jdGlvbmFsaXR5XCIpO1xuXHR9KTtcblxuXHQvLyBDYXJvdXNlbHNcblx0JCgnLmNhcm91c2VsLS10d2l0dGVyJykuc2xpY2soe1xuXHRcdHNsaWRlc1RvU2hvdzogMSxcblx0XHRzbGlkZXNUb1Njcm9sbDogMSxcblx0XHRhdXRvcGxheTogdHJ1ZSxcblx0XHRhdXRvcGxheVNwZWVkOiAyMDAwLFxuXHRcdGRvdHM6IHRydWUsXG5cdFx0YWRhcHRpdmVIZWlnaHQ6IGZhbHNlXG5cdH0pO1xuXHQkKCcuYWNjcmVkaXRhdGlvbnMgLnJvdycpLnNsaWNrKHtcblx0XHRzbGlkZXNUb1Nob3c6IDUsXG5cdFx0c2xpZGVzVG9TY3JvbGw6IDEsXG5cdFx0YXV0b3BsYXk6IHRydWUsXG5cdFx0YXV0b3BsYXlTcGVlZDogMjAwMCxcblx0XHRkb3RzOiBmYWxzZSxcblx0XHRhZGFwdGl2ZUhlaWdodDogZmFsc2UsXG5cdFx0YXJyb3dzOiBmYWxzZSxcblx0XHRpbmZpbml0ZTogdHJ1ZSxcblx0XHRyZXNwb25zaXZlOiBbXG5cdFx0XHR7XG5cdFx0XHRcdGJyZWFrcG9pbnQ6IDEzNjksXG5cdFx0XHRcdHNldHRpbmdzOiB7XG5cdFx0XHRcdFx0c2xpZGVzVG9TaG93OiAzLFxuXHRcdFx0XHRcdHNsaWRlc1RvU2Nyb2xsOiAzLFxuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRicmVha3BvaW50OiA5NjAsXG5cdFx0XHRcdHNldHRpbmdzOiB7XG5cdFx0XHRcdFx0c2xpZGVzVG9TaG93OiAyLFxuXHRcdFx0XHRcdHNsaWRlc1RvU2Nyb2xsOiAyXG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdGJyZWFrcG9pbnQ6IDcyMCxcblx0XHRcdFx0c2V0dGluZ3M6IHtcblx0XHRcdFx0XHRzbGlkZXNUb1Nob3c6IDIsXG5cdFx0XHRcdFx0c2xpZGVzVG9TY3JvbGw6IDJcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0YnJlYWtwb2ludDogNTQwLFxuXHRcdFx0XHRzZXR0aW5nczoge1xuXHRcdFx0XHRcdHNsaWRlc1RvU2hvdzogMSxcblx0XHRcdFx0XHRzbGlkZXNUb1Njcm9sbDogMVxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XVxuXHR9KTtcbn0pO1xuIl19
